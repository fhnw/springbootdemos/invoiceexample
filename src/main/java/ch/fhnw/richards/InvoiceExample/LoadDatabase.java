package ch.fhnw.richards.InvoiceExample;

import ch.fhnw.richards.InvoiceExample.customer.Customer;
import ch.fhnw.richards.InvoiceExample.customer.CustomerRepository;
import ch.fhnw.richards.InvoiceExample.invoice.Invoice;
import ch.fhnw.richards.InvoiceExample.invoice.InvoiceRepository;
import ch.fhnw.richards.InvoiceExample.lineItem.LineItem;
import ch.fhnw.richards.InvoiceExample.lineItem.LineItemRepository;
import ch.fhnw.richards.InvoiceExample.product.Product;
import ch.fhnw.richards.InvoiceExample.product.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
class LoadDatabase implements CommandLineRunner {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private InvoiceRepository invoiceRepository;
    @Autowired
    private LineItemRepository lineItemRepository;

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Override
    public void run(String... args) throws Exception {
        // Initialization code using JPA repositories
        log.info("Preloading " + productRepository.save(new Product(1, "Mouse", 30)));
        log.info("Preloading " + productRepository.save(new Product(2, "Monitor", 250)));
        log.info("Preloading " + productRepository.save(new Product(3, "PC", 1450)));
        log.info("Preloading " + productRepository.save(new Product(4, "Keyboard", 60)));

        Customer c1 = new Customer(1, "fred", "fred@email.com");
        log.info("Preloading " + customerRepository.save(c1));
        Customer c2 = new Customer(2, "anna", "anna@email.com");
        log.info("Preloading " + customerRepository.save(c2));

        Invoice i1 = new Invoice(1, LocalDate.of(2023, 8, 19)); i1.setCustomer(c1);
        log.info("Preloading " + invoiceRepository.save(i1));
        Invoice i2 = new Invoice(2, LocalDate.of(2023, 8, 23)); i2.setCustomer(c1);
        log.info("Preloading " + invoiceRepository.save(i2));
        Invoice i3 = new Invoice(3, LocalDate.of(2023, 8, 21)); i3.setCustomer(c2);
        log.info("Preloading " + invoiceRepository.save(i3));
        Invoice i4 = new Invoice(4, LocalDate.of(2023, 8, 25)); i4.setCustomer(c2);
        log.info("Preloading " + invoiceRepository.save(i4));

        log.info("Preloading " + lineItemRepository.save(new LineItem(1, 1, 1, 30)));
        log.info("Preloading " + lineItemRepository.save(new LineItem(1, 4, 2, 60)));
        log.info("Preloading " + lineItemRepository.save(new LineItem(2, 2, 2, 240)));
        log.info("Preloading " + lineItemRepository.save(new LineItem(2, 3, 1, 1500)));
        log.info("Preloading " + lineItemRepository.save(new LineItem(3, 1, 100, 25)));
        log.info("Preloading " + lineItemRepository.save(new LineItem(4, 2, 10, 200)));
        log.info("Preloading " + lineItemRepository.save(new LineItem(4, 3, 10, 1300)));
    }
}
