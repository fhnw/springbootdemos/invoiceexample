package ch.fhnw.richards.InvoiceExample.invoice;

import ch.fhnw.richards.InvoiceExample.customer.Customer;
import ch.fhnw.richards.InvoiceExample.lineItem.LineItem;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "invoice")
public class Invoice {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer ID;
    @Column(name = "Date")
    private LocalDate date;
    //@Column(name = "customerID")
    //private Integer customerID;
    @ManyToOne
    @JoinColumn(name = "customerID") // Column to define in the invoice-table
    private Customer customer;
    @OneToMany(mappedBy = "invoice", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<LineItem> lineItems = new ArrayList<>();

    public Invoice() {}

    public Invoice(Integer ID, LocalDate date) {
        this.ID = ID;
        this.date = date;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return Objects.equals(ID, invoice.ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID);
    }
}
