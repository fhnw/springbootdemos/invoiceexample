package ch.fhnw.richards.InvoiceExample.invoice;

import ch.fhnw.richards.InvoiceExample.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

}
