package ch.fhnw.richards.InvoiceExample.invoice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InvoiceController {
    private final InvoiceRepository repository;

    InvoiceController(InvoiceRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/invoices")
    List<Invoice> all() {
        return repository.findAll();
    }

    @GetMapping("/invoices/{id}")
    Invoice one(@PathVariable Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new InvoiceNotFoundException(id));
    }
}
