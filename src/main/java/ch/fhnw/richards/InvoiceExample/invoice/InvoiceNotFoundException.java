package ch.fhnw.richards.InvoiceExample.invoice;

public class InvoiceNotFoundException extends RuntimeException {
    InvoiceNotFoundException(Integer id) {
        super("Could not find invoice " + id);
    }
}
