package ch.fhnw.richards.InvoiceExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvoiceExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvoiceExampleApplication.class, args);
	}

}