package ch.fhnw.richards.InvoiceExample.customer;

import ch.fhnw.richards.InvoiceExample.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
