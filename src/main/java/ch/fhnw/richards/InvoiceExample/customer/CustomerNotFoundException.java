package ch.fhnw.richards.InvoiceExample.customer;

public class CustomerNotFoundException extends RuntimeException {
    CustomerNotFoundException(Integer id) {
        super("Could not find customer " + id);
    }
}
