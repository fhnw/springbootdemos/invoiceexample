package ch.fhnw.richards.InvoiceExample.lineItem;

import ch.fhnw.richards.InvoiceExample.invoice.Invoice;
import ch.fhnw.richards.InvoiceExample.product.Product;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

@Entity
@Table(name = "lineitem")
public class LineItem {
    @EmbeddedId
    private LineItemKey id;
    @Column(name = "Quantity")
    private Integer quantity;
    @Column(name = "SalesPrice")
    private Integer salesPrice;
    @ManyToOne
    @JoinColumn(name = "productID", insertable = false, updatable = false)
    private Product product;
    @ManyToOne
    @JoinColumn(name = "invoiceID", insertable = false, updatable = false)
    @JsonBackReference
    private Invoice invoice;

    public LineItem() {}

    public LineItem(int invoiceID, int productID, int quantity, int salesPrice) {
        this.id = new LineItemKey(invoiceID, productID);
        this.quantity = quantity;
        this.salesPrice = salesPrice;
    }

    public LineItemKey getId() {
        return id;
    }

    public void setId(LineItemKey id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(Integer salesPrice) {
        this.salesPrice = salesPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
