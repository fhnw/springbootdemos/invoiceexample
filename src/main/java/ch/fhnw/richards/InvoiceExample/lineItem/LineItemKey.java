package ch.fhnw.richards.InvoiceExample.lineItem;

import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;

/**
 * LineItems have a compound key in the database. To represent this in SpringBoot,
 * we must define a separate class for the ID. Note that that this class must
 * implement equals() and hashCoce() - the Invoice class does not need them.
 */
@Embeddable
public class LineItemKey implements Serializable {
    private Integer invoiceID;
    private Integer productID;

    public LineItemKey() {}

    public LineItemKey(Integer invoiceID, Integer productID) {
        this.invoiceID = invoiceID;
        this.productID = productID;
    }

    public Integer getInvoiceID() {
        return invoiceID;
    }

    public void setInvoiceID(Integer invoiceID) {
        this.invoiceID = invoiceID;
    }

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LineItemKey that = (LineItemKey) o;
        return Objects.equals(invoiceID, that.invoiceID) && Objects.equals(productID, that.productID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(invoiceID, productID);
    }
}
