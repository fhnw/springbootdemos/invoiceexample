package ch.fhnw.richards.InvoiceExample.lineItem;

import ch.fhnw.richards.InvoiceExample.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LineItemRepository extends JpaRepository<LineItem, Integer> {
}
