package ch.fhnw.richards.InvoiceExample.product;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "product")
public class Product {
    @Id @GeneratedValue
    @Column(name = "ID")
    private Integer ID;
    @Column(name = "Name")
    private String name;
    @Column(name = "Price")
    private Integer price;

    public Product() {}

    public Product(Integer ID, String name, Integer price) {
        this.ID = ID;
        this.name = name;
        this.price = price;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(ID, product.ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID);
    }
}
